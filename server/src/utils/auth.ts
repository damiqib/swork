import { NextFunction, Request, Response } from 'express'
import axios from 'axios'

const auth = async (
  req: Request,
  res: Response,
  next: NextFunction
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<Response<any, Record<string, any>> | undefined> => {
  const { 'x-api-key': clockifyApiKey } = req.headers

  if (
    !clockifyApiKey ||
    typeof clockifyApiKey !== 'string' ||
    !(await axios.get('https://api.clockify.me/api/v1/user', {
      headers: { 'X-Api-Key': clockifyApiKey },
    }))
  ) {
    return res.status(404)
  }

  res.locals = {
    ...res.locals,
    clockifyApiKey,
  }

  next()
}

export { auth }
