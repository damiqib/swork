import { isWeekend, isBefore, addDays, format } from 'date-fns'
import Holidays from 'date-holidays'

import { IProjectSchemaData } from '../types/IProjectSchema'

const generateBaseProjectSchema = (year: number): IProjectSchemaData => {
  const holidays = new Holidays('FI')

  const FIRST_DAY_OF_YEAR = new Date(`${year}-01-01`)
  const LAST_DAY_OF_YEAR = new Date(`${year}-12-31`)
  const FIRST_DAY_OF_NEXT_YEAR = addDays(LAST_DAY_OF_YEAR, 1)

  const data = {}
  let day = FIRST_DAY_OF_YEAR

  while (isBefore(day, FIRST_DAY_OF_NEXT_YEAR)) {
    const weekend = isWeekend(day)
    const nationalHoliday = holidays.isHoliday(day)
    const workingDay = !weekend && !nationalHoliday

    data[format(day, 'yyyy-MM-dd')] = {
      date: day,
      isWeekend: weekend,
      isNationalHoliday: nationalHoliday ? true : false,
      isVacation: false,
      isWorkingDay: workingDay,
      dailyHours: workingDay ? 7.5 : 0,
    }

    day = addDays(day, 1)
  }

  return data as IProjectSchemaData
}

export { generateBaseProjectSchema }
