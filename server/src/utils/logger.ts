/* eslint-disable no-console */

const logger = {
  debug: (...params: unknown[]): void => {
    if (process.env.NODE_ENV === 'development') {
      console.log(...params)
    }
  },
  info: (...params: unknown[]): void => {
    console.log(...params)
  },
  error: (...params: unknown[]): void => {
    console.error(...params)
  },
}

export { logger }
