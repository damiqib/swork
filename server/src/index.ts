import mongoose from 'mongoose'

import { app } from './app'
import { logger } from './utils/logger'

const PORT = process.env.PORT || 3231

mongoose
  .connect(<string>process.env.MONGODB_URI, {
    dbName: <string>process.env.MONGODB_DB_NAME,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    logger.info('MongoDB connected.')

    app.listen(<string>PORT, () => {
      logger.info(`Server listening on ${PORT}`)
    })
  })
  .catch((error) => {
    logger.error('Failed to connect to MongoDB. Server not started.', error.message)
  })
