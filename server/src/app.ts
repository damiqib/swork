import express, { Application } from 'express'
import helmet from 'helmet'
import cors from 'cors'
import path from 'path'

import { routes } from './routes'
import { errorHandler } from './utils/errorHandler'
import { unknownEndpoint } from './utils/unknownEndpoint'

const app: Application = express()

// Helmet
app.use(helmet())

// Cors
app.use(cors())

// Set CSP's
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      // eslint-disable-next-line quotes
      defaultSrc: ["'self'", 'https://api.clockify.me'],
      imgSrc: [
        // eslint-disable-next-line quotes
        "'self'",
        'data:',
      ],
    },
  })
)

// JSON
app.use(express.json())

// Set static folder
app.use(express.static(path.join(__dirname, '..', 'public')))

// Routes
app.use('/', routes)

// Unknown endpoint
app.use(unknownEndpoint)

// Error handling
app.use(errorHandler)

export { app }
