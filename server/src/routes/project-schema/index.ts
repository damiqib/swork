import express, { NextFunction, Request, Response } from 'express'

import { auth } from '../../utils/auth'
import { generateBaseProjectSchema } from '../../utils/generateBaseProjectSchema'
import { IApiResponse } from '../../types/IApiResponse'
import { IProjectSchema, IProjectSchemaDoc, IProjectSchemaDay } from '../../types/IProjectSchema'
import { ProjectSchema } from '../../models/projectSchema'

const validateParams = (
  req: Request,
  res: Response,
  next: NextFunction
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Response<any, Record<string, any>> | undefined => {
  const { clockifyProjectId, year } = req.params

  if (
    !clockifyProjectId ||
    typeof clockifyProjectId !== 'string' ||
    !year ||
    typeof year !== 'string'
  ) {
    return res.status(400).json({ error: { message: 'Invalid params!' } })
  }

  res.locals = {
    ...res.locals,
    clockifyProjectId,
    year: parseInt(year),
  }

  next()
}

const projectSchemas = express.Router()

projectSchemas.get(
  '/:clockifyProjectId/:year',
  auth,
  validateParams,
  async (req: Request, res: Response<IApiResponse<IProjectSchema>>) => {
    const { clockifyApiKey, clockifyProjectId, year } = res.locals

    const projectSchema = await ProjectSchema.findOne({ clockifyApiKey, clockifyProjectId, year })

    if (!projectSchema) {
      const generatedProjectSchema = generateBaseProjectSchema(year)

      const newProjectSchema = new ProjectSchema({
        clockifyApiKey,
        clockifyProjectId,
        year,
        data: generatedProjectSchema,
      })

      await newProjectSchema.save()

      return res.status(200).json({
        data: newProjectSchema,
      })
    }

    return res.status(200).json({ data: projectSchema.toJSON() })
  }
)

projectSchemas.patch(
  '/:clockifyProjectId/:year',
  auth,
  validateParams,
  async (req: Request, res: Response<IApiResponse<IProjectSchema>>) => {
    const { clockifyApiKey, clockifyProjectId, year } = res.locals
    const { day, data }: { day: string; data: IProjectSchemaDay } = req.body

    if (!day || !data.date) {
      return res.status(400).json({ error: { message: 'Invalid params!' } })
    }

    const projectSchema = await ProjectSchema.findOne({ clockifyApiKey, clockifyProjectId, year })

    if (!projectSchema) {
      return res.status(400).json({ error: { message: 'Project schema not found!' } })
    }

    const projectSchemaData = { ...projectSchema.data }
    projectSchemaData[day] = data

    projectSchema.markModified('data')

    const updatedProjectSchema = (await ProjectSchema.findByIdAndUpdate(
      projectSchema._id,
      { $set: { data: projectSchemaData } },
      {
        new: true,
      }
    )) as IProjectSchemaDoc

    return res.status(200).json({ data: updatedProjectSchema.toJSON() })
  }
)

export { projectSchemas }
