import express, { NextFunction, Request, Response } from 'express'

import { auth } from '../../utils/auth'
import { IApiResponse } from '../../types/IApiResponse'
import { IProjectSettings, IProjectSettingsDoc } from '../../types/IProjectSettings'
import { ProjectSettings } from '../../models/projectSettings'

const validateParams = (
  req: Request,
  res: Response,
  next: NextFunction
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Response<any, Record<string, any>> | undefined => {
  const { clockifyProjectId } = req.params

  if (!clockifyProjectId || typeof clockifyProjectId !== 'string') {
    return res.status(400).json({ error: { message: 'Invalid params!' } })
  }

  res.locals = {
    ...res.locals,
    clockifyProjectId,
  }

  next()
}

const projectSettings = express.Router()

projectSettings.get(
  '/:clockifyProjectId',
  auth,
  validateParams,
  async (req: Request, res: Response<IApiResponse<IProjectSettings>>) => {
    const { clockifyApiKey, clockifyProjectId, year } = res.locals

    const projectSettings = await ProjectSettings.findOne({
      clockifyApiKey,
      clockifyProjectId,
      year,
    })

    if (!projectSettings) {
      const newProjectSettings = new ProjectSettings({
        clockifyApiKey,
        clockifyProjectId,
        usesSchemas: false,
        startDate: '',
        endDate: '',
        dailyHours: 7.5,
      })

      await newProjectSettings.save()

      return res.status(200).json({ data: newProjectSettings })
    }

    return res.status(200).json({ data: projectSettings.toJSON() })
  }
)

projectSettings.post(
  '/:clockifyProjectId',
  auth,
  validateParams,
  async (req: Request, res: Response<IApiResponse<IProjectSettings>>) => {
    const { clockifyApiKey, clockifyProjectId } = res.locals
    const { usesSchemas, startDate, endDate, dailyHours } = req.body

    const data = {
      clockifyApiKey,
      clockifyProjectId,
      usesSchemas,
      startDate,
      endDate,
      dailyHours,
    }

    const projectSettings = await ProjectSettings.findOne({ clockifyApiKey, clockifyProjectId })

    if (!projectSettings) {
      const newProjectSettings = new ProjectSettings(data)

      await newProjectSettings.save()
      return res.status(200).json({ data: newProjectSettings })
    }

    const updatedProjectSettings = (await ProjectSettings.findByIdAndUpdate(
      projectSettings._id,
      data,
      {
        new: true,
      }
    )) as IProjectSettingsDoc

    return res.status(200).json({ data: updatedProjectSettings.toJSON() })
  }
)

export { projectSettings }
