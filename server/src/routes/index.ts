import express from 'express'

import { projectSettings } from './project-settings'
import { projectSchemas } from './project-schema'

const routes = express.Router()

routes.use('/api/v1/project-settings', projectSettings)
routes.use('/api/v1/project-schema', projectSchemas)

export { routes }
