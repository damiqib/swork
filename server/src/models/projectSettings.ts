import { model, Schema, SchemaDefinitionProperty } from 'mongoose'

import { IProjectSettings, IProjectSettingsDoc } from '../types/IProjectSettings'

const ProjectSettingsSchemaFields: Record<keyof IProjectSettings, SchemaDefinitionProperty> = {
  clockifyApiKey: {
    type: String,
    required: true,
  },
  clockifyProjectId: {
    type: String,
    required: true,
  },
  usesSchemas: {
    type: Boolean,
    required: true,
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  dailyHours: {
    type: Number,
    required: true,
  },
}

const ProjectSettingsSchema = new Schema(ProjectSettingsSchemaFields, {
  timestamps: true,
  toJSON: {
    transform: (_doc, ret): void => {
      ret.id = ret._id.toString()
      delete ret._id
      delete ret.__v
    },
  },
})

const ProjectSettings = model<IProjectSettingsDoc>('ProjectSettings', ProjectSettingsSchema)

export { ProjectSettings }
