import { model, Schema, SchemaDefinitionProperty } from 'mongoose'

import { IProjectSchema, IProjectSchemaDoc } from '../types/IProjectSchema'

const ProjectSchemaSchemaFields: Record<keyof IProjectSchema, SchemaDefinitionProperty> = {
  clockifyApiKey: {
    type: String,
    required: true,
  },
  clockifyProjectId: {
    type: String,
    required: true,
  },
  year: {
    type: Number,
    required: true,
  },
  data: {
    type: Object,
    required: true,
  },
}

const ProjectSchemaSchema = new Schema(ProjectSchemaSchemaFields, {
  timestamps: true,
  toJSON: {
    transform: (_doc, ret): void => {
      ret.id = ret._id.toString()
      delete ret._id
      delete ret.__v
    },
  },
})

const ProjectSchema = model<IProjectSchemaDoc>('ProjectSchema', ProjectSchemaSchema)

export { ProjectSchema }
