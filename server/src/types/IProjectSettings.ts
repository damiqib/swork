import mongoose from 'mongoose'

export interface IProjectSettings {
  clockifyApiKey: string
  clockifyProjectId: string
  usesSchemas: boolean
  startDate?: string
  endDate?: string
  dailyHours: number
}

// IProjectSettings + Mongoose Document
export interface IProjectSettingsDoc extends IProjectSettings, mongoose.Document {}
