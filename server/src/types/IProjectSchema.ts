import mongoose from 'mongoose'

export interface IProjectSchemaDay {
  date: string
  isWeekend: boolean
  isNationalHoliday: boolean
  isVacation: boolean
  isWorkingDay: boolean
  dailyHours: number
}

export interface IProjectSchemaData {
  [key: string]: IProjectSchemaDay
}

export interface IProjectSchema {
  clockifyApiKey: string
  clockifyProjectId: string
  year: number
  data: IProjectSchemaData
}

// IProjectSchema + Mongoose Document
export interface IProjectSchemaDoc extends IProjectSchema, mongoose.Document {}
