# sWork

This personal project maps time entries added to Clockify projects against a work time -schema and shows simple week/month/year stats.

[Clockify API](https://clockify.me/developers-api)

Deployed app can be accessed at [https://swork-production-client.onrender.com/](https://swork-production-server.onrender.com).

!["Swork"](swork.png)

# Setup development environment

## Client

```
$ git clone

$ touch client/.env
  BUILD_PATH=../server/public
  GENERATE_SOURCEMAP=false
  INLINE_RUNTIME_CHUNK=false
  REACT_APP_API_END_POINT=http://localhost:3231/api/v1
  REACT_APP_CLOCKIFY_API_KEY=<your-api-key> # Set only for local development

$ yarn install

$ yarn run dev
```

## Server

```
$ git clone

$ touch server/.env
  PORT=3231
  MONGODB_URI=mongodb+srv://<username>:<password>@cluster0.xxxxx.mongodb.net
  MONGODB_DB_NAME=swork

$ yarn install

$ yarn run dev
```

## Fire up the service using Docker

`docker compose down && docker compose up --build`

## Available Scripts

In the project directory, you can run:

### Client

#### `yarn run dev`

Runs the app in defined port.

#### `yarn test`

Launches the test runner in the interactive watch mode.

#### `yarn build`

Production build.

#### `yarn start`

Starts production build.

#### `yarn run ib:client`

Shorthand for installing and building client, used in Heroku process.

### Server

#### `yarn run dev`

Runs the app in defined port.

#### `yarn run lint:watch`

Eslint in watch mode.

#### `yarn run test:watch`

Launches the test runner in the interactive watch mode.

#### `yarn build`

Production build.

#### `yarn start`

Starts production build.

#### `yarn run ib:server`

Shorthand for installing and building server, used in Heroku process.

## TODO

- [ ]: Dynamic YearSelection - could do query with pageSize of 1 to /time-entries to fetch the oldest entry
- [ ]: When clicking on schema date - open a modal for precise day editing
- [ ]: StatisticsGauges-view for prior to current years
- [ ]: Fix for 'Error: <path> attribute d: Expected number, "M NaN NaN L NaN Na…".'
