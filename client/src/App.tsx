import { useState, useEffect } from 'react'
import axios from 'axios'
import { Accordion } from 'react-bootstrap'

import { IUser } from './interfaces/IUser'
import { ITimeEntry } from './interfaces/ITimeEntry'
import { IApiResponse } from './interfaces/IApiResponse'
import { IProjectSettings, IProjectSettingsUpdate } from './interfaces/IProjectSettings'
import { IProjectSchema } from './interfaces/IProjectSchema'

import { Layout } from './components/Layout'
import { ClockifyApiKey } from './components/ClockifyApiKey'
import { WorkspaceSelect } from './components/WorkspaceSelect'
import { ProjectSelect } from './components/ProjectSelect'
import { TaskSelect } from './components/TaskSelect'
import { TimeEntries } from './components/TimeEntries'
import { Loading } from './components/Loading'
import { StatisticsTable } from './components/StatisticsTable'
import { ProjectSettings } from './components/ProjectSettings'
import { ProjectSchema } from './components/ProjectSchema'
import { YearSelection } from './components/Calendar/YearSelection'
import { StatisticsGauges } from './components/StatisticsGauges'

const App = (): JSX.Element => {
  const [user, setUser] = useState<IUser | null>(null)
  const [workspace, setWorkspace] = useState<string | null>(null)
  const [project, setProject] = useState<string | null>(null)
  const [task, setTask] = useState<string | null>(null)
  const [projectSettings, setProjectSettings] = useState<IProjectSettings | null>(null)
  const [projectSchema, setProjectSchema] = useState<IProjectSchema | null>(null)
  const [timeEntries, setTimeEntries] = useState<ITimeEntry[] | []>([])
  const [year, setYear] = useState<number | null>(null)

  const [clockifyApiKey, setClockifyApiKey] = useState<string | undefined | null>(
    process.env.REACT_APP_CLOCKIFY_API_KEY || window.localStorage.getItem('clockify-api-key')
  )

  const currentYear = new Date().getFullYear()

  // Get user profile
  useEffect(() => {
    if (clockifyApiKey) {
      axios
        .get('https://api.clockify.me/api/v1/user', {
          headers: { 'X-Api-Key': clockifyApiKey },
        })
        .then((response) => {
          if (response.data) {
            setUser(response.data)
          }
        })
        .catch(() => {
          window.localStorage.clear()
          window.location.reload()
        })
    }
  }, [clockifyApiKey])

  // Get project settings
  useEffect(() => {
    if (clockifyApiKey && project) {
      axios
        .get<IApiResponse<IProjectSettings>>(
          `${process.env.REACT_APP_API_END_POINT}/project-settings/${project}`,
          {
            headers: { 'X-Api-Key': clockifyApiKey },
          }
        )
        .then((response) => {
          if (response.data.data) {
            setProjectSettings(response.data.data)
          }
        })
    }
  }, [clockifyApiKey, project])

  // Get project schema
  useEffect(() => {
    if (clockifyApiKey && project && year) {
      axios
        .get<IApiResponse<IProjectSchema>>(
          `${process.env.REACT_APP_API_END_POINT}/project-schema/${project}/${year}`,
          {
            headers: { 'X-Api-Key': clockifyApiKey },
          }
        )
        .then((response) => {
          if (response.data.data) {
            setProjectSchema(response.data.data)
          }
        })
    }
  }, [clockifyApiKey, project, year, setProjectSchema])

  const updateProjectSettings = (settings: IProjectSettingsUpdate): void => {
    axios
      .post<IApiResponse<IProjectSettings>>(
        `${process.env.REACT_APP_API_END_POINT}/project-settings/${project}`,
        settings,
        {
          headers: { 'X-Api-Key': clockifyApiKey },
        }
      )
      .then((response) => {
        if (response.data.data) {
          setProjectSettings(response.data.data)
        }
      })
  }

  if (!clockifyApiKey) {
    return (
      <Layout>
        <ClockifyApiKey setClockifyApiKey={setClockifyApiKey} />
      </Layout>
    )
  }

  if (!user) {
    return (
      <Layout>
        <Loading />
      </Layout>
    )
  }

  return (
    <Layout>
      <h1>Hi {user.name}</h1>

      <WorkspaceSelect
        clockifyApiKey={clockifyApiKey}
        workspace={workspace}
        setWorkspace={setWorkspace}
      />

      {workspace && (
        <ProjectSelect
          clockifyApiKey={clockifyApiKey}
          project={project}
          setProject={setProject}
          workspace={workspace}
        />
      )}

      {workspace && project && (
        <>
          <TaskSelect
            clockifyApiKey={clockifyApiKey}
            workspace={workspace}
            project={project}
            setTask={setTask}
          />

          {projectSettings ? (
            <>
              <ProjectSettings
                projectSettings={projectSettings}
                updateProjectSettings={updateProjectSettings}
              />

              <YearSelection selectedYear={year} setYear={setYear} />

              {year && (
                <>
                  {projectSettings.usesSchemas && (
                    <>
                      {projectSchema && (
                        <>
                          <ProjectSchema
                            clockifyApiKey={clockifyApiKey}
                            project={project}
                            projectSettings={projectSettings}
                            year={year}
                            projectSchema={projectSchema}
                            setProjectSchema={setProjectSchema}
                          />

                          {year === currentYear && (
                            <div className="mt-4">
                              <StatisticsGauges
                                schemaData={projectSchema.data}
                                projectSettings={projectSettings}
                                timeEntries={timeEntries}
                              />
                            </div>
                          )}

                          <div className="mt-4">
                            <Accordion>
                              <Accordion.Item eventKey="0">
                                <Accordion.Header>
                                  Statistics of {timeEntries.length} time entries for the year{' '}
                                  {year}
                                </Accordion.Header>
                                <Accordion.Body>
                                  <StatisticsTable
                                    timeEntries={timeEntries}
                                    schemaData={projectSchema.data}
                                    year={year}
                                    projectSettings={projectSettings}
                                  />
                                </Accordion.Body>
                              </Accordion.Item>
                            </Accordion>
                          </div>
                        </>
                      )}
                    </>
                  )}

                  <div className="mt-4">
                    <Accordion>
                      <Accordion.Item eventKey="0">
                        <Accordion.Header>{timeEntries.length} time entries</Accordion.Header>
                        <Accordion.Body>
                          <TimeEntries
                            clockifyApiKey={clockifyApiKey}
                            user={user}
                            workspace={workspace}
                            project={project}
                            task={task}
                            year={year}
                            timeEntries={timeEntries}
                            setTimeEntries={setTimeEntries}
                          />
                        </Accordion.Body>
                      </Accordion.Item>
                    </Accordion>
                  </div>
                </>
              )}
            </>
          ) : (
            <Loading />
          )}
        </>
      )}
    </Layout>
  )
}

export { App }
