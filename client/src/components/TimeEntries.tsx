import { useEffect } from 'react'
import axios from 'axios'
import { Table } from 'react-bootstrap'
import { intervalToDuration, parseISO, formatDuration, startOfYear, endOfYear } from 'date-fns'
import _ from 'lodash'

import { IUser } from '../interfaces/IUser'
import { ITimeEntry, ITimeInterval } from '../interfaces/ITimeEntry'

import { Loading } from './Loading'

type TimeEntriesProps = {
  clockifyApiKey: string
  user: IUser
  workspace: string
  project: string
  task: string | null
  year: number
  timeEntries: ITimeEntry[] | []
  setTimeEntries: React.Dispatch<React.SetStateAction<ITimeEntry[] | []>>
}

const TimeEntries = ({
  clockifyApiKey,
  user,
  workspace,
  project,
  task,
  year,
  timeEntries,
  setTimeEntries,
}: TimeEntriesProps): JSX.Element => {
  const pageSize = 200

  // Get time entries recursively, Clockify API returns n time entries per page
  useEffect(() => {
    setTimeEntries([])

    const getTimeEntries = (page: number): void => {
      axios
        .get(
          `https://api.clockify.me/api/v1/workspaces/${workspace}/user/${user.id}/time-entries`,
          {
            headers: { 'X-Api-Key': clockifyApiKey },
            params: {
              project,
              task,
              hydrated: true,
              page,
              start: startOfYear(new Date(year, 0, 1)).toISOString(),
              end: endOfYear(new Date(year, 0, 1)).toISOString(),
              'page-size': pageSize,
            },
          }
        )
        .then((response) => {
          if (response.data.length) {
            setTimeEntries((timeEntries) => _.uniqBy([...timeEntries, ...response.data], 'id'))
          }

          // Fetch more "if there's probably more to come"
          if (response.data.length >= pageSize) {
            getTimeEntries(page + 1)
          }
        })
    }

    getTimeEntries(1)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, workspace, project, task, year])

  const getHumanDuration = (timeInterval: ITimeInterval): string => {
    // Handle currently ongoing event
    if (!timeInterval || !timeInterval.end) {
      return 'On-going...'
    }

    return formatDuration(
      intervalToDuration({
        start: parseISO(timeInterval.start),
        end: parseISO(timeInterval.end),
      }),
      { format: ['hours', 'minutes'] }
    )
  }

  if (timeEntries.length === 0) {
    return <Loading />
  }

  return (
    <>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Description</th>
            {!task && <th>Task</th>}
            <th>Start</th>
            <th>End</th>
            <th>Duration</th>
          </tr>
        </thead>
        <tbody>
          {timeEntries.map((timeEntry: ITimeEntry) => (
            <tr key={timeEntry.id}>
              <td>{timeEntry.description}</td>
              {!task && <td>{timeEntry.task?.name}</td>}
              <td>{timeEntry.timeInterval?.start}</td>
              <td>{timeEntry.timeInterval?.end}</td>
              <td>{getHumanDuration(timeEntry.timeInterval)}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  )
}

export { TimeEntries }
