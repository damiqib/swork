import { mount } from 'enzyme'

import { Loading } from './Loading'

describe('Loading', () => {
  it('should render', () => {
    const wrapper = mount(<Loading />)

    expect(wrapper.find('Loading').exists()).toEqual(true)
  })
})
