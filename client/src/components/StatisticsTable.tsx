import { startOfYear, endOfYear, startOfMonth, endOfMonth } from 'date-fns'
import { Table } from 'react-bootstrap'

import { SHORT_MONTHS } from '../utils/calendar'
import { getSchemaHoursForPeriod } from '../utils/schema'
import { getTimeEntriesHoursForPeriod } from '../utils/entry'
import { IProjectSchemaData } from '../interfaces/IProjectSchema'
import { IProjectSettings } from '../interfaces/IProjectSettings'
import { ITimeEntry } from '../interfaces/ITimeEntry'

type StatisticsTableProps = {
  timeEntries: ITimeEntry[]
  schemaData: IProjectSchemaData
  year: number
  projectSettings: IProjectSettings
}

type YearIndexHoursMapType = { [key: number]: { [key: number]: number } }

const StatisticsTable = ({
  timeEntries,
  schemaData,
  year,
  projectSettings,
}: StatisticsTableProps): JSX.Element => {
  const schemaHours: YearIndexHoursMapType = { [year]: {} }
  const entryHours: YearIndexHoursMapType = { [year]: {} }

  // Wrapper for getSchemaHoursForPeriod in order to save schema hours to component
  const getSchemaHours = (index: number): number => {
    const schemaHoursForPeriod = getSchemaHoursForPeriod(
      schemaData,
      projectSettings,
      startOfMonth(new Date(year, index, 1)),
      endOfMonth(new Date(year, index, 1))
    )

    schemaHours[year][index] = schemaHoursForPeriod

    return schemaHoursForPeriod
  }

  // Wrapper for getTimeEntriesHoursForPeriod in order to save entry hours to component
  const getTimeEntriesHours = (index: number): number => {
    const entryHoursForPeriod = getTimeEntriesHoursForPeriod(
      timeEntries,
      startOfMonth(new Date(year, index, 1)),
      endOfMonth(new Date(year, index, 1))
    )

    entryHours[year][index] = entryHoursForPeriod

    return entryHoursForPeriod
  }

  const getTimeEntriesHoursDifferenceToSchemaHoursForPeriod = (index: number): number => {
    return entryHours[year][index] - schemaHours[year][index]
  }

  const getCumulativeTimeEntriesHoursDifferenceToSchemaHoursForPeriod = (index: number): number => {
    let sum = 0

    for (let i = index; i >= 0; i--) {
      sum += getTimeEntriesHoursDifferenceToSchemaHoursForPeriod(i)
    }

    return sum
  }

  return (
    <>
      <Table className="table">
        <thead>
          <tr>
            <th>Month</th>
            <th>Schema hours</th>
            <th>Actual hours</th>
            <th>Difference</th>
            <th>Cumulative difference</th>
          </tr>
        </thead>
        <tbody>
          {Array.apply(null, Array(12)).map((_, index) => (
            <tr key={`month-${index + 1}`}>
              <td>{SHORT_MONTHS[index]}</td>
              <td>{getSchemaHours(index)}</td>
              <td>{getTimeEntriesHours(index)}</td>
              <td>{getTimeEntriesHoursDifferenceToSchemaHoursForPeriod(index)}</td>
              <td>
                {new Date().getMonth() >= index &&
                  getCumulativeTimeEntriesHoursDifferenceToSchemaHoursForPeriod(index)}
              </td>
            </tr>
          ))}
          <tr>
            <th>Total</th>
            <th>
              {getSchemaHoursForPeriod(
                schemaData,
                projectSettings,
                startOfYear(new Date(year, 0, 1)),
                endOfYear(new Date(year, 0, 1))
              )}
            </th>
            <th>
              {getTimeEntriesHoursForPeriod(
                timeEntries,
                startOfYear(new Date(year, 0, 1)),
                endOfYear(new Date(year, 0, 1))
              )}
            </th>
            <th></th>
            <th>
              {getCumulativeTimeEntriesHoursDifferenceToSchemaHoursForPeriod(new Date().getMonth())}
            </th>
          </tr>
        </tbody>
      </Table>
    </>
  )
}

export { StatisticsTable }
