import { useEffect } from 'react'
import classNames from 'classnames'
import { Alert } from 'react-bootstrap'

import './YearSelection.css'

type YearSelectionProps = {
  selectedYear: number | null
  setYear: React.Dispatch<React.SetStateAction<number | null>>
}

const YearSelection = ({ selectedYear, setYear }: YearSelectionProps): JSX.Element => {
  const currentYear = new Date().getFullYear()

  const years = [currentYear - 3, currentYear - 2, currentYear - 1, currentYear, currentYear + 1]

  useEffect(() => {
    setYear(currentYear)
  }, [currentYear, setYear])

  return (
    <Alert variant="warning">
      <div className="year-selector">
        {years.map((year) => (
          <span
            key={year}
            className={classNames('year', {
              selected: selectedYear === year,
            })}
            onClick={(): void => setYear(year)}
          >
            {year}
          </span>
        ))}
      </div>
    </Alert>
  )
}

export { YearSelection }
