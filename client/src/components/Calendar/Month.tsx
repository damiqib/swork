import { startOfMonth, endOfMonth, differenceInDays, getDay } from 'date-fns'

import { IProjectSettings } from '../../interfaces/IProjectSettings'
import { IProjectSchema } from '../../interfaces/IProjectSchema'

import { Day } from './Day'

type MonthProps = {
  year: number
  month: number
  onDayClick: (date: string) => void
  projectSettings: IProjectSettings
  projectSchema: IProjectSchema
}

const Month = ({
  year,
  month,
  onDayClick,
  projectSettings,
  projectSchema,
}: MonthProps): JSX.Element => {
  const FIRST_DAY_OF_MONTH = startOfMonth(new Date(`${year}-${month}-01`))
  const LAST_DAY_OF_MONTH = endOfMonth(new Date(`${year}-${month}-01`))
  const DAYS_IN_MONTH = differenceInDays(LAST_DAY_OF_MONTH, FIRST_DAY_OF_MONTH) + 1
  const WEEKDAY_OF_FIRST_DAY_IN_MONTH = getDay(FIRST_DAY_OF_MONTH)

  return (
    <>
      {Array.apply(null, Array(WEEKDAY_OF_FIRST_DAY_IN_MONTH)).map((_, index) => (
        <td key={`padder-${index}`}></td>
      ))}

      {Array.apply(null, Array(DAYS_IN_MONTH)).map((_, index) => (
        <Day
          key={index}
          year={year}
          month={month}
          day={index + 1}
          onDayClick={onDayClick}
          projectSettings={projectSettings}
          projectSchema={projectSchema}
        />
      ))}
    </>
  )
}

export { Month }
