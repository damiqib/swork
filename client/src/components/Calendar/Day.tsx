import { isToday, format } from 'date-fns'
import classNames from 'classnames'

import { isBeforeSchema, isAfterSchema } from '../../utils/schema'
import { IProjectSettings } from '../../interfaces/IProjectSettings'
import { IProjectSchema } from '../../interfaces/IProjectSchema'

import { Loading } from '../Loading'

import './Day.css'

type DayProps = {
  year: number
  month: number
  day: number
  onDayClick: (date: string) => void
  projectSettings: IProjectSettings
  projectSchema: IProjectSchema
}

const Day = ({
  year,
  month,
  day,
  onDayClick,
  projectSettings,
  projectSchema,
}: DayProps): JSX.Element => {
  const date = new Date(year, month - 1, day)
  const today = isToday(date)
  const formattedDate = format(date, 'yyyy-MM-dd')

  const dateData = projectSchema.data[formattedDate]

  if (!dateData) {
    return (
      <td>
        <Loading />
      </td>
    )
  }

  return (
    <td
      key={`day-${day}`}
      className={classNames('day', {
        weekend: dateData.isWeekend,
        'national-holiday': dateData.isNationalHoliday,
        vacation: dateData.isVacation,
        'before-schema': isBeforeSchema(projectSettings, date),
        'after-schema': isAfterSchema(projectSettings, date),
        today,
      })}
      onClick={(): void => onDayClick(formattedDate)}
    >
      {day}
    </td>
  )
}

export { Day }
