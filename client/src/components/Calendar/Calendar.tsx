import React from 'react'

import { SHORT_MONTHS, SHORT_WEEKDAYS } from '../../utils/calendar'
import { IProjectSettings } from '../../interfaces/IProjectSettings'
import { IProjectSchema } from '../../interfaces/IProjectSchema'

import { Month } from './Month'

import './Calendar.css'

type CalendarProps = {
  year: number
  onDayClick: (date: string) => void
  projectSettings: IProjectSettings
  projectSchema: IProjectSchema
}

const Calendar = ({
  year,
  onDayClick,
  projectSettings,
  projectSchema,
}: CalendarProps): JSX.Element => {
  return (
    <>
      <table className="schema-calendar">
        <thead>
          <tr>
            <th></th>
            {Array.apply(null, Array(6)).map((_, index) => (
              <React.Fragment key={`weekday-header-upper-${index}`}>
                {SHORT_WEEKDAYS.map((WEEKDAY, index) => (
                  <th key={`weekday-header-${index}`}>{WEEKDAY}</th>
                ))}
              </React.Fragment>
            ))}
          </tr>
        </thead>

        <tbody>
          {Array.apply(null, Array(12)).map((_, index) => (
            <tr key={`month-${index + 1}`}>
              <td>{SHORT_MONTHS[index]}</td>
              <Month
                year={year}
                month={index + 1}
                onDayClick={onDayClick}
                projectSettings={projectSettings}
                projectSchema={projectSchema}
              />
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export { Calendar }
