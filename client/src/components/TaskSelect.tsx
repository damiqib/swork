import { useState, useEffect } from 'react'
import axios from 'axios'
import { Form } from 'react-bootstrap'

import { ITask } from '../interfaces/ITask'

import { Loading } from './Loading'

type TaskSelectProps = {
  clockifyApiKey: string
  workspace: string
  project: string | null
  setTask: React.Dispatch<React.SetStateAction<string | null>>
}

const TaskSelect = ({
  clockifyApiKey,
  workspace,
  project,
  setTask,
}: TaskSelectProps): JSX.Element | null => {
  const pageSize = 200
  const [tasks, setTasks] = useState<ITask[] | null>(null)

  // Get tasks
  useEffect(() => {
    axios
      .get(`https://api.clockify.me/api/v1/workspaces/${workspace}/projects/${project}/tasks`, {
        headers: { 'X-Api-Key': clockifyApiKey },
        params: {
          'page-size': pageSize,
        },
      })
      .then((response) => {
        setTasks(response.data)
      })
  }, [clockifyApiKey, workspace, project, setTasks])

  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setTask(event.target.value ? event.target.value : null)
  }

  // Clear task when project is changed
  useEffect(() => {
    setTask(null)
  }, [project, setTask])

  if (tasks && tasks.length === 0) return null

  return (
    <Form.Group controlId="form.Task" className="mt-2">
      <Form.Label>Task</Form.Label>
      {project && tasks ? (
        <>
          <Form.Control as="select" onChange={onChangeHandler}>
            <option value={undefined}></option>
            {tasks
              .sort((a, b) => (a.name > b.name ? 1 : -1))
              .map((_task) => (
                <option value={_task.id} key={_task.id}>
                  {_task.name}
                </option>
              ))}
          </Form.Control>
        </>
      ) : (
        <Loading />
      )}
    </Form.Group>
  )
}

export { TaskSelect }
