import GaugeChart from 'react-gauge-chart'

type StatisticsGaugeProps = {
  percent: number
  period: string
}

/**
 * 80 % is left end (red zone)
 * 95 % is on top of cauge (center point)
 * 110 % is right edge (green zone)
 */
const StatisticsGauge = ({ percent, period }: StatisticsGaugeProps): JSX.Element => {
  const low = 0.8
  const high = 1.1

  // Caps edges to prevent cauge overflowing
  // it's horizontal bounds
  const calculatePercent = (percent: number): number => {
    const cappedPercent = (percent - low) / (high - low)

    if (cappedPercent < -0.05) return -0.05
    if (cappedPercent > 1.05) return 1.05

    return cappedPercent
  }

  return (
    <GaugeChart
      id={`gauge-${period}`}
      nrOfLevels={20}
      percent={calculatePercent(percent)}
      style={{
        width: 150,
      }}
      colors={['#FF0000', '#00FF00']}
      hideText={true}
      animate={false}
    />
  )
}

export { StatisticsGauge }
