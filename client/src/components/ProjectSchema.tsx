import { useState } from 'react'
import axios from 'axios'
import Switch from 'react-switch'

import { IApiResponse } from '../interfaces/IApiResponse'
import { IProjectSchema, IProjectSchemaDay } from '../interfaces/IProjectSchema'
import { IProjectSettings } from '../interfaces/IProjectSettings'

import { Calendar } from './Calendar'

type ProjectSchemaProps = {
  clockifyApiKey: string
  project: string | null
  projectSettings: IProjectSettings
  year: number
  projectSchema: IProjectSchema
  setProjectSchema: React.Dispatch<React.SetStateAction<IProjectSchema | null>>
}

const ProjectSchema = ({
  clockifyApiKey,
  project,
  projectSettings,
  year,
  projectSchema,
  setProjectSchema,
}: ProjectSchemaProps): JSX.Element => {
  const [markAsVacation, setMarkAsVacation] = useState<boolean>(false)

  const updateDay = (date: string, data: IProjectSchemaDay): void => {
    axios
      .patch<IApiResponse<IProjectSchema>>(
        `${process.env.REACT_APP_API_END_POINT}/project-schema/${project}/${year}`,
        {
          day: date,
          data,
        },
        {
          headers: { 'X-Api-Key': clockifyApiKey },
        }
      )
      .then((response) => {
        if (response.data.data) {
          setProjectSchema(response.data.data)
        }
      })
  }

  const onDayClick = (date: string): void => {
    if (!projectSchema || !projectSchema.data[date]) {
      throw new Error('Invalid onDayClick!')
    }

    const dayData = projectSchema.data[date]

    if (markAsVacation) {
      const isVacation = !dayData.isVacation

      const data: IProjectSchemaDay = {
        ...projectSchema.data[date],
        isVacation,
        dailyHours:
          dayData.isWeekend || dayData.isNationalHoliday || isVacation
            ? 0
            : projectSettings.dailyHours,
        isWorkingDay: dayData.isWeekend || dayData.isNationalHoliday ? false : !isVacation,
      }

      updateDay(date, data)
    }
  }

  return (
    <>
      <Calendar
        year={year}
        onDayClick={onDayClick}
        projectSettings={projectSettings}
        projectSchema={projectSchema}
      />

      <div style={{ display: 'flex', alignItems: 'center' }}>
        <span id="mark-as-vacation" style={{ paddingRight: '1em' }}>
          Mark vacations
        </span>

        <Switch
          className="react-switch"
          onChange={(value: boolean): void => setMarkAsVacation(value)}
          checked={markAsVacation}
          aria-labelledby="mark-as-vacation"
        />
      </div>
    </>
  )
}

export { ProjectSchema }
