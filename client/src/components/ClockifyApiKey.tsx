import { useState } from 'react'
import { Form, Button } from 'react-bootstrap'
import axios from 'axios'

type ClockifyApiKeyProps = {
  setClockifyApiKey: React.Dispatch<React.SetStateAction<string | undefined | null>>
}

const ClockifyApiKey = ({ setClockifyApiKey }: ClockifyApiKeyProps): JSX.Element => {
  const [userAddedClockifyApiKey, setUserAddedClockifyApiKey] = useState<string>('')
  const [userAddedClockifyApiKeyIsInvalid, setUserAddedClockifyApiKeyIsInvalid] =
    useState<boolean>(false)

  const testClockifyApiKey = (event: React.MouseEvent<HTMLElement>): void => {
    event.preventDefault()

    axios
      .get('https://api.clockify.me/api/v1/user', {
        headers: { 'X-Api-Key': userAddedClockifyApiKey },
      })
      .then(() => {
        window.localStorage.setItem('clockify-api-key', userAddedClockifyApiKey)
        setClockifyApiKey(userAddedClockifyApiKey)
      })
      .catch(() => {
        setUserAddedClockifyApiKeyIsInvalid(true)
      })
  }

  return (
    <Form noValidate>
      <Form.Group className="mb-3" controlId="clockify-api-key">
        <Form.Control
          type="text"
          placeholder="Enter Clockify API Key"
          value={userAddedClockifyApiKey}
          onChange={(event): void => setUserAddedClockifyApiKey(event.target.value)}
          isInvalid={userAddedClockifyApiKeyIsInvalid}
        />
        <Form.Control.Feedback type="invalid">
          Given Clockify API key is not valid.
        </Form.Control.Feedback>
        <Form.Text className="text-muted">The key is only saved on your browser.</Form.Text>
      </Form.Group>

      <Button variant="primary" type="submit" onClick={testClockifyApiKey}>
        Use API Key
      </Button>
    </Form>
  )
}

export { ClockifyApiKey }
