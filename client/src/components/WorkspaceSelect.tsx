import { useState, useEffect } from 'react'
import axios from 'axios'
import { Form } from 'react-bootstrap'

import { IWorkspace } from '../interfaces/IWorkspace'

import { Loading } from './Loading'

type WorkspaceSelectProps = {
  clockifyApiKey: string
  workspace: string | null
  setWorkspace: React.Dispatch<React.SetStateAction<string | null>>
}

const WorkspaceSelect = ({
  clockifyApiKey,
  workspace,
  setWorkspace,
}: WorkspaceSelectProps): JSX.Element => {
  const [workspaces, setWorkspaces] = useState<IWorkspace[] | null>(null)

  // Get workspaces
  useEffect(() => {
    axios
      .get('https://api.clockify.me/api/v1/workspaces', {
        headers: { 'X-Api-Key': clockifyApiKey },
      })
      .then((response) => {
        // Set first returned workspace as a default workspace
        if (response.data.length) {
          setWorkspace(response.data[0].id)
        }

        setWorkspaces(response.data)
      })
  }, [clockifyApiKey, setWorkspace])

  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setWorkspace(event.target.value)
  }

  if (!workspaces) {
    return <Loading />
  }

  if (workspaces.length === 1) {
    return <></>
  }

  return (
    <Form.Group controlId="form.workspace" className="mb-4">
      <Form.Label>Workspace</Form.Label>
      {workspace && workspaces && (
        <>
          <Form.Control as="select" defaultValue={workspace} onChange={onChangeHandler}>
            {workspaces.map((_workspace) => (
              <option value={_workspace.id} key={_workspace.id}>
                {_workspace.name}
              </option>
            ))}
          </Form.Control>
        </>
      )}
    </Form.Group>
  )
}

export { WorkspaceSelect }
