import { useState, useEffect } from 'react'
import axios from 'axios'
import { Form } from 'react-bootstrap'

import { IProject } from '../interfaces/IProject'

import { Loading } from './Loading'

type ProjectSelectProps = {
  clockifyApiKey: string
  project: string | null
  setProject: React.Dispatch<React.SetStateAction<string | null>>
  workspace: string
}

const ProjectSelect = ({
  clockifyApiKey,
  project,
  setProject,
  workspace,
}: ProjectSelectProps): JSX.Element => {
  const pageSize = 200
  const [projects, setProjects] = useState<IProject[] | null>(null)

  // Get projects
  useEffect(() => {
    axios
      .get(`https://api.clockify.me/api/v1/workspaces/${workspace}/projects`, {
        headers: { 'X-Api-Key': clockifyApiKey },
        params: {
          'page-size': pageSize,
        },
      })
      .then((response) => {
        // Set first returned project as a default project
        if (response.data.length) {
          setProject(response.data[0].id)
        }

        setProjects(response.data)
      })
  }, [clockifyApiKey, setProject, workspace])

  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setProject(event.target.value)
  }

  return (
    <Form.Group controlId="form.Project">
      <Form.Label>Project</Form.Label>
      {project && projects ? (
        <>
          <Form.Control as="select" defaultValue={project} onChange={onChangeHandler}>
            {projects.map((_project) => (
              <option value={_project.id} key={_project.id}>
                {_project.name}
              </option>
            ))}
          </Form.Control>
        </>
      ) : (
        <Loading />
      )}
    </Form.Group>
  )
}

export { ProjectSelect }
