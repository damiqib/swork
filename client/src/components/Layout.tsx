import { Container, Row } from 'react-bootstrap'

type LayoutProps = {
  children: React.ReactNode
}

const Layout = ({ children }: LayoutProps): JSX.Element => {
  return (
    <Container>
      <Row className="mt-4">{children}</Row>
    </Container>
  )
}

export { Layout }
