import { Spinner } from 'react-bootstrap'

const Loading = (): JSX.Element => {
  return <Spinner animation="grow" variant="info" size="sm" />
}

export { Loading }
