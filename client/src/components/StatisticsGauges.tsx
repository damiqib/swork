import { sub, startOfWeek, getISOWeek, getMonth } from 'date-fns'

import {
  getSchemaHoursForPeriod,
  getSchemaHoursForCurrentWeekUntilCurrentDate,
  getSchemaHoursForCurrentMonthUntilCurrentDate,
  getSchemaHoursForCurrentYearUntilCurrentDate,
} from '../utils/schema'
import {
  getTimeEntriesHoursForPeriod,
  getTimeEntriesHoursForCurrentWeekUntilCurrentDate,
  getTimeEntriesHoursForCurrentMonthUntilCurrentDate,
  getTimeEntriesHoursForCurrentYearUntilCurrentDate,
} from '../utils/entry'
import { LONG_MONTHS } from '../utils/calendar'
import { IProjectSchemaData } from '../interfaces/IProjectSchema'
import { IProjectSettings } from '../interfaces/IProjectSettings'
import { ITimeEntry } from '../interfaces/ITimeEntry'

import { StatisticsGauge } from './StatisticsGauge'

type StatisticsGaugeProps = {
  schemaData: IProjectSchemaData
  projectSettings: IProjectSettings
  timeEntries: ITimeEntry[]
}

const StatisticsGauges = ({
  schemaData,
  projectSettings,
  timeEntries,
}: StatisticsGaugeProps): JSX.Element => {
  const getCurrentTimeStatus = (
    unit: 'week' | 'month' | 'year'
  ): { schema: number; actual: number } => {
    let schema = 0
    let actual = 0

    switch (unit) {
      case 'week':
        schema = getSchemaHoursForCurrentWeekUntilCurrentDate(schemaData, projectSettings)
        actual = getTimeEntriesHoursForCurrentWeekUntilCurrentDate(timeEntries)
        break

      case 'month':
        schema = getSchemaHoursForCurrentMonthUntilCurrentDate(schemaData, projectSettings)
        actual = getTimeEntriesHoursForCurrentMonthUntilCurrentDate(timeEntries)
        break

      case 'year':
        schema = getSchemaHoursForCurrentYearUntilCurrentDate(schemaData, projectSettings)
        actual = getTimeEntriesHoursForCurrentYearUntilCurrentDate(timeEntries)
        break
    }

    return { schema, actual }
  }

  return (
    <div
      className="d-flex flex-row justify-content-evenly"
      style={{
        backgroundColor: '#fff3cd',
        padding: '0.7em',
        color: '#826c28',
        textAlign: 'center',
        borderRadius: '8px',
      }}
    >
      <div>
        Week {getISOWeek(sub(new Date(), { weeks: 1 }))}
        <StatisticsGauge
          percent={
            getTimeEntriesHoursForPeriod(
              timeEntries,
              startOfWeek(sub(new Date(), { weeks: 1 }), { weekStartsOn: 1 }),
              new Date()
            ) /
            getSchemaHoursForPeriod(
              schemaData,
              projectSettings,
              startOfWeek(sub(new Date(), { weeks: 1 }), { weekStartsOn: 1 }),
              new Date()
            )
          }
          period="last-week"
        />
      </div>
      <div>
        Week {getISOWeek(new Date())} so far
        <StatisticsGauge
          percent={getCurrentTimeStatus('week').actual / getCurrentTimeStatus('week').schema}
          period="week"
        />
      </div>
      <div>
        {LONG_MONTHS[getMonth(new Date())]} so far
        <StatisticsGauge
          percent={getCurrentTimeStatus('month').actual / getCurrentTimeStatus('month').schema}
          period="month"
        />
      </div>
      <div>
        {new Date().getFullYear()} so far
        <StatisticsGauge
          percent={getCurrentTimeStatus('year').actual / getCurrentTimeStatus('year').schema}
          period="year"
        />
      </div>
    </div>
  )
}

export { StatisticsGauges }
