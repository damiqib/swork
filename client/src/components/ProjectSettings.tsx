import { useState, useEffect } from 'react'
import { Formik, Field, Form, FormikHelpers } from 'formik'
import { format, parseISO } from 'date-fns'

import { IProjectSettings, IProjectSettingsUpdate } from '../interfaces/IProjectSettings'

import { Loading } from './Loading'

type ProjectSettingsProps = {
  projectSettings: IProjectSettings
  updateProjectSettings: (settings: IProjectSettingsUpdate) => void
}

const ProjectSettings = ({
  projectSettings,
  updateProjectSettings,
}: ProjectSettingsProps): JSX.Element => {
  const [initialValues, setInitialValues] = useState<Required<IProjectSettingsUpdate> | null>(null)

  useEffect(() => {
    setInitialValues({
      usesSchemas: projectSettings.usesSchemas,
      startDate: projectSettings.startDate
        ? format(parseISO(projectSettings.startDate), 'yyyy-MM-dd')
        : '',
      endDate: projectSettings.endDate
        ? format(parseISO(projectSettings.endDate), 'yyyy-MM-dd')
        : '',
      dailyHours: projectSettings.dailyHours,
    })
  }, [projectSettings])

  if (!initialValues) return <Loading />

  return (
    <>
      <Formik
        initialValues={initialValues}
        enableReinitialize
        onSubmit={(
          values: Required<IProjectSettingsUpdate>,
          { setSubmitting }: FormikHelpers<Required<IProjectSettingsUpdate>>
        ): void => {
          updateProjectSettings(values)
          setSubmitting(false)
        }}
      >
        {({ dirty, isSubmitting, values }): JSX.Element => (
          <Form className="row gx-3 gy-1 mb-3 align-items-end">
            <div className="col-auto">
              <div className="form-check">
                <Field
                  type="checkbox"
                  name="usesSchemas"
                  className="form-check-input"
                  disabled={values.usesSchemas}
                />
                <label className="form-check-label">Uses Schemas</label>
              </div>
            </div>

            <div className="col-sm-3">
              <label htmlFor="startDate">Project start date</label>
              <Field id="startDate" name="startDate" type="date" className="form-control" />
            </div>
            <div className="col-sm-3">
              <label htmlFor="endDate">Project end date</label>
              <Field id="endDate" name="endDate" type="date" className="form-control" />
            </div>
            <div className="col-sm-3">
              <label htmlFor="dailyHours">Daily base hours</label>
              <Field id="dailyHours" name="dailyHours" type="number" className="form-control" />
            </div>
            <div className="col-auto">
              <button type="submit" className="btn btn-primary" disabled={!dirty || isSubmitting}>
                Save
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  )
}

export { ProjectSettings }
