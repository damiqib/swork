export interface IProjectSchemaDay {
  date: string
  isWeekend: boolean
  isNationalHoliday: boolean
  isVacation: boolean
  isWorkingDay: boolean
  dailyHours: number
}

export interface IProjectSchemaData {
  [key: string]: IProjectSchemaDay
}

export interface IProjectSchema {
  clockifyApiKey: string
  clockifyProjectId: string
  year: number
  data: IProjectSchemaData
}
