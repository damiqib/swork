export interface ITask {
  id: string
  name: string
  projectId: string
  status: string
}
