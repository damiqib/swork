export interface IUser {
  id: string
  name: string
  email: string
  defaultWorkspace: string
  activeWorkspace: string
  settings: {
    dateFormat: string
    myStartOfDay: string
    timeFormat: string
    timeZone: string
    weekStart: string
  }
}
