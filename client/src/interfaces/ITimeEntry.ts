import { IProject } from './IProject'
import { IUser } from './IUser'
import { ITask } from './ITask'
import { ITag } from './ITag'

export interface ITimeEntry {
  id: string
  description: string
  project: IProject
  projectId: string
  tagIds: ITag[] | [] | null
  user: IUser
  userId: string
  workspaceId: string
  task?: ITask
  taskId?: string | null
  billable: boolean
  isLocked: boolean
  timeInterval: ITimeInterval
}

export interface ITimeInterval {
  duration: string
  start: string
  end: string
}
