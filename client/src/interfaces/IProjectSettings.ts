export interface IProjectSettings {
  clockifyApiKey: string
  clockifyProjectId: string
  usesSchemas: boolean
  startDate?: string
  endDate?: string
  dailyHours: number
}

export type IProjectSettingsUpdate = Pick<
  IProjectSettings,
  'usesSchemas' | 'startDate' | 'endDate' | 'dailyHours'
>
