import { add, sub, startOfDay } from 'date-fns'

import { IProjectSettings } from '../interfaces/IProjectSettings'
import { isBeforeSchema, isAfterSchema } from './schema'

describe('schema.ts', () => {
  describe('isBeforeSchema', () => {
    const baseMockProjectSettings: IProjectSettings = {
      clockifyApiKey: '',
      clockifyProjectId: '',
      usesSchemas: false,
      startDate: '',
      endDate: '',
      dailyHours: 7.5,
    }

    let startOfToday = startOfDay(new Date())

    it('should return false when startDate is not defined in projectSettings', () => {
      const mockProjectSettings = { ...baseMockProjectSettings, startDate: '' }

      expect(isBeforeSchema(mockProjectSettings, startOfToday)).toEqual(false)
    })

    it('should return false when given date is after schema startDate', () => {
      const mockProjectSettings = {
        ...baseMockProjectSettings,
        startDate: sub(startOfToday, { seconds: 1 }).toISOString(),
      }

      expect(isBeforeSchema(mockProjectSettings, startOfToday)).toEqual(false)
    })

    it('should return false when given date is on the same date as schema startDate', () => {
      const mockProjectSettings = {
        ...baseMockProjectSettings,
        startDate: add(startOfToday, { seconds: 1 }).toISOString(),
      }

      expect(isBeforeSchema(mockProjectSettings, startOfToday)).toEqual(false)
    })

    it('should return true when given date is before schema startDate', () => {
      const mockProjectSettings = {
        ...baseMockProjectSettings,
        startDate: add(startOfToday, { days: 1 }).toISOString(),
      }

      expect(isBeforeSchema(mockProjectSettings, startOfToday)).toEqual(true)
    })
  })

  describe('isAfterSchema', () => {
    const baseMockProjectSettings: IProjectSettings = {
      clockifyApiKey: '',
      clockifyProjectId: '',
      usesSchemas: false,
      startDate: '',
      endDate: '',
      dailyHours: 7.5,
    }

    let startOfToday = startOfDay(new Date())

    it('should return false when endDate is not defined in projectSettings', () => {
      const mockProjectSettings = { ...baseMockProjectSettings, endDate: '' }

      expect(isAfterSchema(mockProjectSettings, startOfToday)).toEqual(false)
    })

    it('should return false when given date is before schema endDate', () => {
      const mockProjectSettings = {
        ...baseMockProjectSettings,
        endDate: add(startOfToday, { seconds: 1 }).toISOString(),
      }

      expect(isAfterSchema(mockProjectSettings, startOfToday)).toEqual(false)
    })

    it('should return true when given date is after schema endDate', () => {
      const mockProjectSettings = {
        ...baseMockProjectSettings,
        endDate: sub(startOfToday, { seconds: 1 }).toISOString(),
      }

      expect(isAfterSchema(mockProjectSettings, startOfToday)).toEqual(true)
    })
  })
})
