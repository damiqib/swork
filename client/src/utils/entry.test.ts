import { sub, startOfDay } from 'date-fns'

import { ITimeEntry } from '../interfaces/ITimeEntry'
import { getTimeEntriesHoursForPeriod } from './entry'

describe('entry.ts', () => {
  const startOfToday = startOfDay(new Date())

  describe('getTimeEntriesHoursForPeriod', () => {
    it('should return sum of hours for a time period', () => {
      const mockEntries: Pick<ITimeEntry, 'timeInterval'>[] = [
        {
          // Adds two hours
          timeInterval: {
            start: sub(new Date(), { days: 1, hours: 2 }).toISOString(),
            end: sub(new Date(), { days: 1 }).toISOString(),
            duration: '',
          },
        },
        {
          // Adds three hours
          timeInterval: {
            start: sub(new Date(), { days: 2, hours: 3 }).toISOString(),
            end: sub(new Date(), { days: 2 }).toISOString(),
            duration: '',
          },
        },
        {
          // Out of time period - ignored
          timeInterval: {
            start: sub(new Date(), { days: 3 }).toISOString(),
            end: sub(new Date(), { days: 3 }).toISOString(),
            duration: '',
          },
        },
      ]

      expect(
        getTimeEntriesHoursForPeriod(mockEntries, sub(startOfToday, { days: 2 }), startOfToday)
      ).toEqual(5)
    })

    it('should return hours for an ongoing event', () => {
      const mockEntries: Pick<ITimeEntry, 'timeInterval'>[] = [
        {
          // Adds two hours
          timeInterval: {
            start: sub(new Date(), { hours: 2, minutes: 31 }).toISOString(),
            end: '',
            duration: '',
          },
        },
      ]

      expect(getTimeEntriesHoursForPeriod(mockEntries, startOfToday, startOfToday)).toEqual(3)
    })
  })
})
