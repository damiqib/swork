import {
  isBefore,
  isAfter,
  isSameDay,
  intervalToDuration,
  parseISO,
  startOfYear,
  startOfMonth,
  startOfWeek,
} from 'date-fns'

import { ITimeEntry } from '../interfaces/ITimeEntry'

/**
 * Returns Math.ceil(duration / 60)
 *
 * eg. 121 minutes -> 3
 */
export const getTimeEntriesHoursForPeriod = (
  timeEntries: Pick<ITimeEntry, 'timeInterval'>[],
  start: Date,
  end: Date
): number => {
  const duration = timeEntries.reduce(
    (sum: number, timeEntry: Pick<ITimeEntry, 'timeInterval'>) => {
      const currentDate = parseISO(timeEntry.timeInterval.start)

      if (
        (isAfter(currentDate, start) || isSameDay(currentDate, start)) &&
        (isBefore(currentDate, end) || isSameDay(currentDate, end))
      ) {
        const duration = intervalToDuration({
          start: parseISO(timeEntry.timeInterval.start),
          // Handle also the on-going event
          end: timeEntry.timeInterval.end ? parseISO(timeEntry.timeInterval.end) : new Date(),
        })

        const durationInMinutes =
          (duration.days ? duration.days * 24 : 0) +
          (duration.hours ? duration.hours * 60 : 0) +
          (duration.minutes ? duration.minutes : 0)
        return sum + durationInMinutes
      } else {
        return sum
      }
    },
    0
  )

  return Math.ceil(duration / 60)
}

export const getTimeEntriesHoursForCurrentYearUntilCurrentDate = (
  timeEntries: ITimeEntry[]
): number => {
  return getTimeEntriesHoursForPeriod(timeEntries, startOfYear(new Date()), new Date())
}

export const getTimeEntriesHoursForCurrentMonthUntilCurrentDate = (
  timeEntries: ITimeEntry[]
): number => {
  return getTimeEntriesHoursForPeriod(timeEntries, startOfMonth(new Date()), new Date())
}

export const getTimeEntriesHoursForCurrentWeekUntilCurrentDate = (
  timeEntries: ITimeEntry[]
): number => {
  return getTimeEntriesHoursForPeriod(
    timeEntries,
    startOfWeek(new Date(), { weekStartsOn: 1 }),
    new Date()
  )
}
