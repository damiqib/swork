import {
  isBefore,
  isAfter,
  isSameDay,
  parse,
  startOfYear,
  startOfMonth,
  startOfWeek,
} from 'date-fns'

import { IProjectSettings } from '../interfaces/IProjectSettings'
import { IProjectSchemaData } from '../interfaces/IProjectSchema'

export const isBeforeSchema = (projectSettings: IProjectSettings, date: Date): boolean => {
  if (!projectSettings.startDate) return false

  return (
    isBefore(date, new Date(projectSettings.startDate)) &&
    !isSameDay(date, new Date(projectSettings.startDate))
  )
}

export const isAfterSchema = (projectSettings: IProjectSettings, date: Date): boolean => {
  if (!projectSettings.endDate) return false

  return isAfter(date, new Date(projectSettings.endDate))
}

export const getSchemaHoursForPeriod = (
  schemaData: IProjectSchemaData,
  projectSettings: IProjectSettings,
  start: Date,
  end: Date
): number => {
  return Object.keys(schemaData).reduce((sum: number, date: string) => {
    const currentDate = parse(date, 'yyyy-MM-dd', new Date())

    if (
      (isAfter(currentDate, start) || isSameDay(currentDate, start)) &&
      (isBefore(currentDate, end) || isSameDay(currentDate, end)) &&
      !isBeforeSchema(projectSettings, currentDate) &&
      !isAfterSchema(projectSettings, currentDate)
    ) {
      return sum + schemaData[date].dailyHours
    } else {
      return sum
    }
  }, 0)
}

export const getSchemaHoursForCurrentYearUntilCurrentDate = (
  schemaData: IProjectSchemaData,
  projectSettings: IProjectSettings
): number => {
  return getSchemaHoursForPeriod(schemaData, projectSettings, startOfYear(new Date()), new Date())
}

export const getSchemaHoursForCurrentMonthUntilCurrentDate = (
  schemaData: IProjectSchemaData,
  projectSettings: IProjectSettings
): number => {
  return getSchemaHoursForPeriod(schemaData, projectSettings, startOfMonth(new Date()), new Date())
}

export const getSchemaHoursForCurrentWeekUntilCurrentDate = (
  schemaData: IProjectSchemaData,
  projectSettings: IProjectSettings
): number => {
  return getSchemaHoursForPeriod(
    schemaData,
    projectSettings,
    startOfWeek(new Date(), { weekStartsOn: 1 }),
    new Date()
  )
}
